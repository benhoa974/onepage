
# Économie 
Territiore ÉCONOMIE POSITIVE

Continuons autrement, en respectant l’humain, la nature et la finitude des ressources.
La Nature comme exemple, rien est appropriable, tout est circulaire.

## L’argent Comme moyen non comme but
- Entreprise sociale et solidaire
- Monnaie locale
- Financement Participatif
- Banque coopérative (La Nef)
- Rémunration innovante : Liberapay, Gratipay 

## LES COMMUNS
- L’économie du partage et pas le partage de l’économie

## Repenser le TRAVAIL
- L’humain est une richesse pas une ressource
- Dissocier travail et revenu (revenu de base) 
- Garantir à tous les besoins vitaux(se nourrir, se loger, se vêtir)
- Apprendre à se connaître avant de travailler et se soumettre
- Inventons le travail de demain 

## ECONOMIE RESPECTUEUSE
- Répartition équitable des richesses
- Economie circulaire
- Consommation locale

## REssources
- Biomimétisme : S'inspirer de la nature plutôt que la consumer
- Le Soleil : une ressource inépuisable 
- La Nature , un système de production perpétuel, auto respectueux
- Open Source : accès Libre au savoir et la création 
- L’Homme abuse de ses droits naturel

## GOUVERNANCE
- Open Système, Horizontalité et transparence
- Sociocratie
- Communalisme , écologie radical
- Explorer et experimenter
- Changeons le Système pas le Climat

## Education 
- Formation sur les pratiques de l'économie circulaire 
- Apprentissage des outils numeriques et citoyens 
- Utlisation et philosophie du Libre et Partage (Logiciel Libre, Wikipedia..)

'''
Nous devons pouvoir vivre pour travailler et non plus travailler pour vivre. Notre économie doit aller dans ce sens.  Mais pour cela nous devons passer par une introspection intérieure : de quoi avons-nous réellement besoin pour être heureux ? Pouvoir alors répondre à ces besoins librement et équitablement. Le temps nous est alors offert, à nous d’en faire ce qu’on l’on souhaite. L’argent n’aura alors plus beaucoup de valeur, la vrai valeur sera le temps de ma personne que je  vais donner  à œuvrer pour des communs. 
'''

## Proverbe
N’oublions pas qu’on empreinte la terre à nos enfants 
